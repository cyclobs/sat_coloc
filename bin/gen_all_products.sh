#Génère tous les produits de coloc (appelle coloc_infos puis xargs gen_coloc_product.py)
#!/bin/bash

# Generate the .netcdf products for all the co-located acquisitions.

# Usage #
# -p for the path under which to save plots (required)
# -r for the path under which to save reports (required)
# -n the number of line to take from the SAR acquisition report taken as input. The acquisitions in that file are
# written from the most recent to the oldest. So by taking the 10 first line, we take the 10 latest acquisition. This
# parameter is optional
set -x
env | grep CONDA
which python

GETOPT=$(getopt -n $0 -o t:c:n:p:r:h:: --long help -- "$@")
eval set -- "$GETOPT"

sar_commit="None"
sar_config="None"

while true; do
    case "$1" in
        -t) sar_commit=$2 ; shift 2 ;;
        -c) sar_config=$2 ; shift 2 ;;
        -p) product_path=$2 ; shift 2 ;;
        -r) report_path=$2 ; shift 2 ;;
        -n) line_number=$2 ; shift 2 ;;
        -- ) shift ; break ;;
        * ) echo "unknown opt $1" >&2 ; exit 1 ;;
    esac
done

script_dir=$(dirname $(readlink -f "$0")) # ie $CONDA_PREFIX/bin

# # Retrieve the current SAR path (the commit can change the path).
# proc_dir=$(dirname $(curl -s http://www.ifremer.fr/cerweb/shoc/shoc_dailyupdate_names.html | grep 'http-equiv="refresh"' | sed -r 's/.*URL=(.*)".*/\1/' | xargs pathUrl.py --schema=dmz -p))

# # Retrieve the acquisitions file list from the most recent commit.
# file_list="$script_dir/coloc_files_list.txt"
# file_list =$(`python $script_dir/coloc_infos.py`)
csv_path=/tmp/api_data/
csv_filepath="$csv_path/api_data.csv"
file_list="$csv_path/filenames.txt"
mkdir -p /tmp/api_data/
coloc_infos.py -pc "$csv_filepath" -pf "$file_list" -st "$sar_commit" -sc "$sar_config" # > $script_dir/coloc_files_list.txt  # removed because of some prints in owi.lightToL2C and owi.L2CtoLight functions
# file_list=$(<$script_dir/coloc_files_list.txt)
# echo "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
# echo $file_list | head 10
# If line_number (-n) is not specified, updating all data

if [ -z "$line_number" ]; then
  echo "Computing analysis for ALL co-located acquisitions"
  <"$file_list" xargs -L 1 -P 10 -I {} $script_dir/gen_coloc_product_wrapper.sh "$product_path" {} "$report_path" "$csv_filepath"
else
  echo "Creating product for $line_number latest acquisitions"
  first_lines=$(tail -n $line_number "$file_list")
  echo "$first_lines" | xargs -L 1 -P 10 -I {} $script_dir/gen_coloc_product_wrapper.sh "$product_path" {} "$report_path" "$csv_filepath"
fi
