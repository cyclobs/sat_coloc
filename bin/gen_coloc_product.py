#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import logging
import argparse

logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)

# Génère le produit de coloc pour 1 couple d’acquis’.
# Prend en entrée les 2 filenames à colocaliser.
# Refait une requête à l’API spécifique pour obtenir les infos plus complètes

if __name__ == "__main__":
    description = "Generation of all co-located data products"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-f", "--sat_files", action="store", required=True,
                        help="Satellite files")
    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path under which save the generated files (netcdf)")
    parser.add_argument("-pc", "--path-csv", action="store", required=True,
                        help="Path to csv containing coloc API data.")
    #TODO be able to provide csv data directly as script arguments, to be able to launch independently from API for 1 file
    #TODO be able to pass 1 file to --path arg
    parser.add_argument("--debug", action="store_true", default=False,
                            help="Enable debug logging level.")

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    #Main process
    import owi
    import os
    from sat_coloc.comparison_processor import coloc_process_save

    netcdf_path = os.path.join(args.path, "product/")
    os.makedirs(netcdf_path, exist_ok=True)

    files = args.sat_files.split(",")
    filename1 = files[0]
    filename2 = files[1]
    status = coloc_process_save(netcdf_path, filename1, filename2, args.path_csv, debug=args.debug, file_logging=True)
    sys.exit(status)
