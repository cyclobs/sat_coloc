#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import pandas as pd
import pathurl
import owi
import os
import argparse

logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)


# Get all couples of co-located images filenames from cyclobs API
def get_coloc_filenames(sar_commit, sar_config, api_host):
    # We use cyclobs API to get a list of the co-located sat images and put them in a dataset
    request_url = f"{api_host}/app/api/getData?coloc_input=C-Band_SAR,L-Band_Radiometer&" \
                  "coloc_comparison=C-Band_SAR,L-Band_Radiometer&include_cols=all"
    df = pd.read_csv(request_url)

    path_mod_instruments = ["C-Band SAR"]
    #conversion of nc file url into their paths
    df["data_url"] = df["data_url"].apply(lambda x: pathurl.toPath(x, schema="dmz"))

    # Modify SAR config and/or commit in path if requested. This is to be able to use data from
    # wanted sarwing listing.
    if sar_commit is not None:
        df.loc[df["instrument_short"].isin(path_mod_instruments), "data_url"] = \
            df.loc[df["instrument_short"].isin(path_mod_instruments), "data_url"].apply(lambda x: x.replace(x.split("/")[9], sar_commit))
    if sar_config is not None:
        df.loc[df["instrument_short"].isin(path_mod_instruments), "data_url"] = \
            df.loc[df["instrument_short"].isin(path_mod_instruments), "data_url"].apply(lambda x: x.replace(x.split("/")[10], sar_config))

    df["data_path"] = df["data_url"]

    df_ret = df.copy(deep=True)

    df = df.drop_duplicates(subset=["data_path", "coloc_index"])

    #sort files paths by name, it will sort them by type too
    df = df.sort_values("data_path")

    #group dataframe elements by coloc_index, concatenation of their paths
    df = df.groupby(["coloc_index"])["data_path"].apply(','.join).reset_index().sort_values("coloc_index", ascending = False)

    filenames_list = df["data_path"].to_numpy()

    for i in range(len(filenames_list)):
        if "SM_OPER_MIR" in filenames_list[i] and not "smap" in filenames_list[i]:
            files = filenames_list[i].split(",")
            file = owi.lightToL2C(files[1])
            file = owi.L2CtoLight(file, resolution=50, mode="ll_gd")
            filenames_list[i] = files[0]+","+file
        elif "smap" in filenames_list[i] and not "SM_OPER_MIR" in filenames_list[i]:
            files = filenames_list[i].split(",")
            file = owi.lightToL2C(files[1])
            file = owi.L2CtoLight(file, resolution=40, mode="ll_gd")
            filenames_list[i] = files[0]+","+file

    return df_ret, filenames_list


if __name__ == "__main__":
    description = "Get API data for all colocs and save it as csv"
    parser = argparse.ArgumentParser(description=description)

    def none_or_str(value):
        if value == 'None':
            return None
        return value

    parser.add_argument("-pc", "--path-csv", action="store", required=True,
                        help="The file path under which save the CSV file. Directory must exist.")
    parser.add_argument("-pf", "--path-filenames", action="store", required=True,
                        help="The file path under which save the filenames file. Directory must exist.")
    parser.add_argument("-st", "--sar-commit", action="store", required=False, type=none_or_str,
                        help="The SAR commit to use.")
    parser.add_argument("-sc", "--sar-config", action="store", required=False, default=None, type=none_or_str,
                        help="The SAR config to use.")
    parser.add_argument("--api-host", action="store", required=False, default="https://cyclobs.ifremer.fr",
                        help="Host to reach for cyclobs API")
    parser.add_argument("--debug", action="store_true", required=False, default=None,
                            help="Enable debug logging level.")

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    #os.makedirs(netcdf_path, exist_ok=True)
    df, filenames = get_coloc_filenames(args.sar_commit, args.sar_config, args.api_host)
    df.to_csv(args.path_csv)

    print()
    with open(args.path_filenames, "w+") as f:
        for filename in filenames:
            f.write("%s\n" % filename)
