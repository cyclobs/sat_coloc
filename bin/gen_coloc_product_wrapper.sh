#!/bin/bash

# A wrapper to write stdout to log file and to write the exit status to file too.
outDir=$1
owiFile=$2
reportDir=$3
csv_filepath=$4

file1=${owiFile%%,*}
file1=${file1##*/}
file2=${owiFile##*/}
echo "${file1}-${file2}"

owibase=$(basename "sat_coloc_${file1}_${file2}")
mkdir -p "$reportDir"
mkdir -p "$reportDir/logs"
mkdir -p "$reportDir/status"

gen_coloc_product.py -p "$outDir" -f "$owiFile" --path-csv "$csv_filepath" > "$reportDir/logs/sat_coloc_${file1}_${file2}.log" 2>&1
status=$?
echo $status > "$reportDir/status/sat_coloc_${file1}_${file2}.status"

cat "$reportDir/logs/sat_coloc_${file1}_${file2}.log"

exit $status
