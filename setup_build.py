from setuptools import setup
import glob

setup(name='sat_coloc',
      description='Production of co-localization files from 2 SAT datafiles',
      url='https://gitlab.ifremer.fr/cyclobs/sat_coloc',
      author="Bastien Anfray",
      author_email="bastien.anfray@ifremer.fr",
      license='GPL',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      packages=['sat_coloc'],
      zip_safe=False,
      scripts=glob.glob('bin/**'),
      )
