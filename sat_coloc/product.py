from fileinput import filename
from os import path
import xarray as xr
import pandas as pd
from datetime import datetime
import collections
import warnings
import numpy as np
import math
import argparse
import dask
import logging
import owi
import pkg_resources
import os

logger = logging.getLogger(__name__)
version = "0.0.2"#pkg_resources.get_distribution("sat_coloc").version

#Merge the 2 datasets of the common zone and create a netcdf file
def create_coloc_netcdf(dataset1, dataset2, filename1, filename2, df_api, path):
    logger.info("Renaming datasets data_vars and attrs.")
    #rename data variables by adding 1 or 2 to distinguish both dataset variables
    for i in dataset1.data_vars :
        dataset1 = dataset1.rename_vars({i : i+"1"})
    for i in dataset2.data_vars :
        dataset2 = dataset2.rename_vars({i : i+"2"})
    for i in dataset1.attrs :
        if i != "polygon_common_zone":
            dataset1 = dataset1.assign_attrs({i: "1", i+"1" : dataset1.attrs[i]})
    for i in dataset2.attrs :
        dataset2 = dataset2.assign_attrs({i: "2", i+"2" : dataset2.attrs[i]})
    logger.info("Done renaming datasets data_vars and attrs.")

    #avoiding a bug when merging datasets
    if 'time' in dataset1.coords:
        logger.info("Squeezing dataset1 time dimension.")
        dataset1 = dataset1.squeeze('time')
        logger.info("Done squeezing dataset1 time dimension.")
    if 'time' in dataset2.coords:
        logger.info("Squeezing dataset2 time dimension.")
        dataset2 = dataset2.squeeze('time')
        logger.info("Done squeezing dataset2 time dimension.")


    #merge both datasets into one
    logger.info("Merging datasets.")
    logger.info(filename1)
    logger.info(filename2)
    new_dataset = xr.merge([dataset1, dataset2], combine_attrs="drop_conflicts", compat="override", join="override")
    logger.info("Done merging datasets.")

    #get filename without path
    new_filename1 = filename1.rsplit("/", 1)[1]
    if new_filename1.endswith('.nc'):
        new_filename1 = new_filename1[:-3]
    new_filename2 = filename2.rsplit("/", 1)[1]

    new_dataset = new_dataset.rio.write_crs("EPSG:4326", inplace=True)
    new_dataset = set_attrs_netcdf(new_dataset, filename1, filename2, df_api)

    if new_dataset.counted_points > 100:
        #create netcdf
        logger.info("NetCDF creation.")
        new_dataset.to_netcdf(path + "sat_coloc_" + new_filename1 + "__" + new_filename2, format = "NETCDF4")
        print("netcdf_created : " + path + "sat_coloc_" + new_filename1 + "__" + new_filename2)
        logger.info("NetCDF created.")
        return 0
    else:
        logger.info("NetCDF not created. Number of common wind_speed datas < 100")
        return 10


#set other netcdf attributes
def set_attrs_netcdf(dataset, filename1, filename2, df_api):
    filenames = filename1+filename2
    id_f2 = filename2
    if "SM_OPER_MIR" in filenames and not "smap" in filenames:
        id_f2 = "-".join(filename2.split("-")[4:6])
        #filename2 = owi.lightToL2C(filename2)
        #filename2 = owi.L2CtoLight(filename2, resolution=3, mode="ll_gd")
    elif "smap" in filenames and not "SM_OPER_MIR" in filenames:
        id_f2 = "-".join(filename2.split("-")[4:6])
        #filename2 = owi.lightToL2C(filename2)
        #filename2 = owi.L2CtoLight(filename2, resolution=3, mode="ll_gd")
    logger.info("Setting new attributes to dataset.")
    #get storm id, cyclone name, basin, vmax and maximum cyclone category from the API
    df_api["filename"] = df_api["data_url"].apply(lambda x: os.path.basename(x))
    df_api["id_f"] = df_api["filename"].apply(lambda x: "-".join(x.split("-")[4:6]))
    #df = df_api.loc[(df_api["filename"] == os.path.basename(filename1)) | (df_api["filename"] == os.path.basename(filename2))]
    df = df_api.loc[(df_api["filename"] == os.path.basename(filename1)) | (df_api["id_f"] == id_f2)]
    df_row = df.iloc[0]

    storm_id = df_row["sid"]
    cyclone_name = df_row["cyclone_name"]
    basins = ""
    vmax = df_row["vmax (m/s)"]
    maximum_cyclone_category = df_row["maximum_cyclone_category"]
    area_intersection = df_row["area_intersect"]
    datetimestart1 = datetime.strptime(df_row["acquisition_start_time"], "%Y-%m-%d %H:%M:%S")
    dist_center1 = int(int(df_row["dist_eye_centroid"])/1000)

    #sometimes an acquisition has multiple basins so the api returned it many times
    for index, row in df.iterrows():
        datetimestart2 = datetime.strptime(df["acquisition_start_time"][index], "%Y-%m-%d %H:%M:%S")
        dist_center2 = int(int(df["dist_eye_centroid"][index])/1000)
        if datetimestart2 != datetimestart1:
            break
        if basins == "":
            basins = str(df["basin"][index])
        else:
            basins += ", " + str(df["basin"][index])

    # Converts wind speed (int) (knots or m/s) to cyclone category
    def spd2cat(spd, unit="knots"):
        """
        Converts wind speed (int) in m/s into cyclone category

        Parameters
        ----------
        spd : int Wind speed in meters per second.

        Returns
        -------
        str
            The cyclone category (dep, storm, cat-X)
        """
        # Dict to convert from string categories to their corresponding maximal wind_speed
        # See also function spd2cat
        maxspd = collections.OrderedDict([
            ("dep", 34),
            ("storm", 64),
            ("cat-1", 82),
            ("cat-2", 95),
            ("cat-3", 112),
            ("cat-4", 136),
            ("cat-5", 999)])

        # Convert to knot
        if unit == "m/s":
            spd = spd * 1.94384
        return list(maxspd.keys())[[i for i, m in enumerate(maxspd) if maxspd[m] - spd > 0][0]]

    cyclone_category = spd2cat(vmax, unit="m/s")

    #time difference between acquisitions
    time_diff = abs(datetimestart1 - datetimestart2)
    time_diff_str = str(time_diff)

    #number of points used for calculation of bias and standard deviation
    sum_wind_speed = dataset["wind_speed1"] + dataset["wind_speed2"]
    #when additioning or substracting a value with a nan, it becomes a nan
    counted_points = np.count_nonzero(~np.isnan(sum_wind_speed))

    # Determine informations analysis for the wind speed
    dict_ws_analysis = {}
    if counted_points > 100:
        # Determine the bias  # (m/s)
        # https://numpy.org/doc/stable/reference/generated/numpy.nanmean.html#numpy.nanmean
        dict_ws_analysis["Bias"] = np.nanmean(dataset["wind_speed1"] - dataset["wind_speed2"])
        # Determine the standard deviation  # (m/s)
        # https://numpy.org/doc/stable/reference/generated/numpy.nanstd.html#numpy.nanstd
        dict_ws_analysis["Standard deviation"] = np.nanstd(
            dataset["wind_speed1"] - dataset["wind_speed2"])

        #Compute the Pearson correlation coefficient between two DataArray objects along a shared dimension.
        correlation_coefficient = xr.corr(dataset["wind_speed1"], dataset["wind_speed2"]).values
    else:
        dict_ws_analysis["Bias"] = 0
        dict_ws_analysis["Standard deviation"] = 0
        correlation_coefficient  = 0


    def calculate_scatter_index(dataarray1, dataarray2):
        #calculate the scatter index with the wind values
        #calcultate RMSE first 
        MSE = np.square(np.subtract(dataarray1, dataarray2)).mean()
        RMSE = math.sqrt(MSE)

        #calculate mean value of all observations
        obs = xr.concat([dataarray1, dataarray2], dim="z")
        mean_obs = np.mean(obs).values

        #calculate scatter index in percentage
        scatter_index = RMSE/mean_obs * 100
        return scatter_index
    
    scatter_index = calculate_scatter_index(dataset["wind_speed1"], dataset["wind_speed2"])


    dataset = dataset.assign_attrs({
                "storm_id" : storm_id,
                "cyclone_name" : cyclone_name,
                "basins" : basins,
                "vmax_m_s" : vmax,
                "current_cyclone_category" : cyclone_category,
                "maximum_cyclone_category" : maximum_cyclone_category,
                "time_difference" : time_diff_str,
                "dist_center1" : int(dist_center1),
                "dist_center2" : int(dist_center2),
                "area_intersection" : area_intersection,
                "bias" : dict_ws_analysis["Bias"],
                "standard_deviation" : dict_ws_analysis["Standard deviation"],
                "counted_points" : counted_points,
                "correlation_coefficient" : correlation_coefficient,
                "scatter_index" : scatter_index,
                "version" : version
                })
    logger.info("Done setting new attributes to dataset.")
    return dataset
