import sys
import logging

logger = logging.getLogger(__name__)

import os
import os.path
from pathlib import Path
import xarray as xr
import pandas as pd
from shapely.geometry import Polygon, Point
import shapely
import shapely.wkt
import numpy as np
import rasterio
from affine import Affine
from cyclobs_utils.coloc_config import MissionConfig, SARConfig, LBandConfig

from xarray.core import dataset
from sat_coloc.product import create_coloc_netcdf


# This program makes the comparison process between 2 co-located netcdf files


# Resample datasets in the same shape
def coloc_resample(filename1: str, filename2: str):
    logger.info("Starting resampling.")

    logger.info("Opening datasets.")
    # open netCDF files in datasets
    dataset1 = xr.open_dataset(filename1)
    logger.info("dataset1 opened.")
    dataset2 = xr.open_dataset(filename2)
    logger.info("dataset2 opened.")

    logger.info("Renaming datasets coordinates into xy.")
    # replace lon and lat with x and y (necessary to make reproject_match() working)
    dataset1 = dataset1.rename({'lon': 'x', 'lat': 'y'})
    dataset2 = dataset2.rename({'lon': 'x', 'lat': 'y'})
    logger.info("Done renaming datasets coordinates into xy.")

    x1 = dataset1.dims["x"]
    y1 = dataset1.dims["y"]
    x2 = dataset2.dims["x"]
    y2 = dataset2.dims["y"]

    pixel_spacing_lon1 = dataset1.coords["x"][1] - dataset1.coords["x"][0]
    pixel_spacing_lat1 = dataset1.coords["y"][1] - dataset1.coords["y"][0]
    pixel_spacing_lon2 = dataset2.coords["x"][1] - dataset2.coords["x"][0]
    pixel_spacing_lat2 = dataset2.coords["y"][1] - dataset2.coords["y"][0]

    logger.info(
        "Modifying dataset coordinates in range 0-360 if coordinates do not cross Greenwich Meridian (lon = 0).")
    if (dataset1.x[0].values < 0 and dataset1.x[len(dataset1.x) - 1].values > 180) or (
            dataset2.x[0].values < 0 and dataset2.x[len(dataset2.x) - 1].values > 180):
        meridian_datasets = True
        logger.info("datasets cross Greenwich Meridian, dataset coordinated will be modified after reprojection.")
    else:
        dataset1["x"] = dataset1["x"] % 360
        dataset2["x"] = dataset2["x"] % 360
        meridian_datasets = False
    logger.info("Done modifying dataset coordinates.")

    logger.info("Reprojecting dataset with higher resolution to make it match with the lower resolution one.")
    if pixel_spacing_lon1 * pixel_spacing_lat1 <= pixel_spacing_lon2 * pixel_spacing_lat2:
        # reproject_match uses by default a nearest interpolation, which is not a problem here because
        # for SAR-SMOS/SMAP colocation we used 40 or 50km resolution for SAR products.
        # As a result, at this point SAR is already at the same resolution as SMOS and SMAP, but the pixel
        # spacing is different.
        # reproject_match is in fact only used to match pixels that are already at the same resolution
        dataset1 = dataset1.rio.reproject_match(dataset2)
        reprojected_dataset = "dataset1"
        logger.info("dataset1 reprojected")
    else:
        dataset2 = dataset2.rio.reproject_match(dataset1)
        reprojected_dataset = "dataset2"
        logger.info("dataset2 reprojected.")
    logger.info("Done reprojecting dataset.")

    if meridian_datasets == True:
        logger.info("Modifying dataset coordinated in range 0-360 if coordinates cross Greenwich Meridian (lon = 0).")
        dataset1["x"] = dataset1["x"] % 360
        dataset2["x"] = dataset2["x"] % 360
        logger.info("Done modifying dataset coordinates.")

    logger.info("Renaming dataset coordinates into lon-lat.")
    dataset1 = dataset1.rename({'x': 'lon', 'y': 'lat'})
    dataset2 = dataset2.rename({'x': 'lon', 'y': 'lat'})
    logger.info("Done renaming dataset coordinates into lon-lat.")

    logger.info("Done resampling.")
    return dataset1, dataset2, reprojected_dataset


# get a polynom of the common zone of the 2 datasets
def get_common_zone(dataset1, dataset2, reprojected_dataset, filename1, filename2):
    logger.info("Starting getting common zone.")
    logger.info("Loading footprints into polygons.")
    poly1 = shapely.wkt.loads(dataset1.footprint)
    poly2 = shapely.wkt.loads(dataset2.footprint)
    logger.info("Done loading footprints into polygons.")

    # transform polygons in range 0-360
    def shape360(lon, lat):
        """shapely shape to 0 360 (for shapely.ops.transform)"""
        orig_type = type(lon)
        lon = np.array(lon) % 360
        return tuple([(orig_type)(lon), lat])

    logger.info("Modifying polygons coords range into 0-360.")
    poly1 = shapely.ops.transform(lambda x, y, z=None: shape360(x, y), poly1)
    poly2 = shapely.ops.transform(lambda x, y, z=None: shape360(x, y), poly2)
    logger.info("Done modifying polygons coords range into 0-360.")

    logger.info("Getting intersection of polygons.")
    poly_intersection = poly1.intersection(poly2)
    logger.info("Done getting intersection of polygons.")

    # SARMissions =["SENTINEL-1 A", "SENTINEL-1 B", "RADARSAT-2"]
    # LBANDMissions = ["SMOS", "SMAP"]
    # if reprojected_dataset == "dataset1" :
    #     mission = dataset1.missionName
    # elif reprojected_dataset == "dataset2" :
    #     mission = dataset2.missionName

    # if mission in SARMissions:
    #     mission_config = SARConfig()
    # elif mission in LBANDMissions:
    #     mission_config = LBandConfig()

    logger.info("Calculating geometry_mask of reprojected dataset.")
    if reprojected_dataset == "dataset1":
        geometry_mask = rasterio.features.geometry_mask([poly_intersection],
                                                        out_shape=(dataset1.lat.shape[0], dataset1.lon.shape[0]),
                                                        transform=get_transform(dataset1, dataset1.lat, dataset1.lon),
                                                        invert=True, all_touched=True)
    elif reprojected_dataset == "dataset2":
        geometry_mask = rasterio.features.geometry_mask([poly_intersection],
                                                        out_shape=(dataset2.lat.shape[0], dataset2.lon.shape[0]),
                                                        transform=get_transform(dataset2, dataset2.lon, dataset2.lat),
                                                        invert=True, all_touched=True)
    logger.info("Done calculating geometry_mask of reprojected dataset.")

    logger.info("Applying geometry_mask on datasets.")
    # Keep only values in common zone
    dataset1_common_zone = dataset1.where(geometry_mask == True)
    dataset2_common_zone = dataset2.where(geometry_mask == True)
    logger.info("Done applying geometry_mask on datasets.")

    logger.info("Reshaping datasets to keep common zone.")
    # reshape to reduce dataset size (avoid having too much non-necessary nan)
    # find lon_min, lon_max, lat_min and lat_max
    lon2D, lat2D = np.meshgrid(dataset1_common_zone.lon, dataset1_common_zone.lat)
    lon2D[~geometry_mask] = np.nan
    lat2D[~geometry_mask] = np.nan
    # We need to round these values to avoid a bug which can occur when merging datasets
    lon_min = round(np.nanmin(lon2D), 6)
    lon_max = round(np.nanmax(lon2D), 6)
    lat_min = round(np.nanmin(lat2D), 6)
    lat_max = round(np.nanmax(lat2D), 6)
    # reshape
    dataset1_common_zone = dataset1_common_zone.where(
        (dataset1_common_zone.lon > lon_min) & (dataset1_common_zone.lon < lon_max) & (
                    dataset1_common_zone.lat > lat_min) & (dataset1_common_zone.lat < lat_max), drop=True)
    dataset2_common_zone = dataset2_common_zone.where(
        (dataset2_common_zone.lon > lon_min) & (dataset2_common_zone.lon < lon_max) & (
                    dataset2_common_zone.lat > lat_min) & (dataset2_common_zone.lat < lat_max), drop=True)
    dataset1_common_zone = dataset1_common_zone.assign_attrs({"polygon_common_zone": str(poly_intersection)})
    logger.info("Done rehaping datasets to keep common zone.")

    logger.info("Modifying datasets coords in range -180,180.")
    dataset1_common_zone = dataset1_common_zone.assign_coords(lon=(((dataset1_common_zone.lon + 180) % 360) - 180))
    dataset2_common_zone = dataset2_common_zone.assign_coords(lon=(((dataset2_common_zone.lon + 180) % 360) - 180))
    logger.info("Modifying datasets coords in range -180,180.")

    logger.info("Done getting common zone.")
    return dataset1_common_zone, dataset2_common_zone


# get transform parameter for some functions, e.g rasterio.features.geometry_mask
def get_transform(sat_ds, lon, lat):
    pixel_spacing_lon = sat_ds.coords["lon"][1] - sat_ds.coords["lon"][0]
    pixel_spacing_lat = sat_ds.coords["lat"][1] - sat_ds.coords["lat"][0]

    transform = Affine(pixel_spacing_lon, 0.0, sat_ds.coords["lon"][0].values,
                       0.0, pixel_spacing_lat, sat_ds.coords["lat"][0].values)
    return transform


def coloc_process_save(path, filename1, filename2, csv_api_coloc, debug=False, file_logging=True):
    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logger = logging.getLogger(os.path.basename(__file__))
    logger.info(f"Processing {filename1} and {filename2} ...")

    try:
        dataset1, dataset2, reprojected_dataset = coloc_resample(filename1, filename2)
        dataset1_common_zone, dataset2_common_zone = get_common_zone(dataset1, dataset2, reprojected_dataset, filename1,
                                                                     filename2)

        df = pd.read_csv(csv_api_coloc)
        status = create_coloc_netcdf(dataset1_common_zone, dataset2_common_zone, filename1, filename2, df, path)

        if status == 0:
            return 0
        elif status == 10:
            logger.error(f"Failed to create netCDF file ! Not enough common points. (status {status}). "
                         f"Files {filename1} and {filename2}")
            return status
        else:
            logger.error(f"Failed to create netCDF file ! Status {status}. "
                         f"Files {filename1} and {filename2}")
            return status

    except Exception as e:
        logger.exception(f"Exception: {e}. Files {filename1} and {filename2}")
        return 1
